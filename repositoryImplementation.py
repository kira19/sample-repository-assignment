import sqlite3

def get_connection():
    return sqlite3.connect('ripimpDB.sqlite')

class StoreException(Exception):
    def __init__(self, message, *errors):
        Exception.__init__(self, message)
        self.errors = errors

class User():
    def __init__(self, name):
        self.name = name

class Car():
    def __init__(self, name):
        self.name = name

# base store class
class Store():
    def __init__(self):
        try:
            self.conn = get_connection()
        except Exception as e:
            raise StoreException(*e.args, **e.kwargs)
        self._complete = False

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        # can test for type and handle different situations
        self.close()

    def complete(self):
        self._complete = True

    def close(self):
        if self.conn:
            try:
                if self._complete:
                    self.conn.commit()
                else:
                    self.conn.rollback()
            except Exception as e:
                raise StoreException(*e.args)
            finally:
                try:
                    self.conn.close()
                except Exception as e:
                    raise StoreException(*e.args)

# store for User obects
class UserStore(Store):

    def add_user(self, user):
        try:
            c = self.conn.cursor()
            
            c.execute('INSERT INTO user (name) VALUES(?)', (user.name,))
        except Exception as e:
            raise StoreException('error storing user')


# store for Car obects
class CarStore(Store):

    def add_car(self, car):
        try:
            c = self.conn.cursor()
            
            c.execute('INSERT INTO car (name) VALUES(?)', (car.name,))
        except Exception as e:
            raise StoreException('error storing car')

# do some assigning vallue
try:
    with UserStore() as user_store:
        user_store.add_user(User('Kirubel'))
        user_store.complete()

    with CarStore() as car_store:
        car_store.add_car(Car('Toyota'))
        car_store.add_car(Car('Mercedes Benz'))
        car_store.add_car(Car('Ford'))
        car_store.add_car(Car('Audi '))
        car_store.complete()
except StoreException as e:
    # exception handling printing
    print(e)
